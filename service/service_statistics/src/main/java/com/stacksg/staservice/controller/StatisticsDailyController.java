package com.stacksg.staservice.controller;

import com.stacksg.commonutils.R;
import com.stacksg.staservice.service.StatisticsDailyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 网站统计日数据 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2020-09-16
 */
@RestController
@RequestMapping("/staservice/statistics")
@CrossOrigin
public class StatisticsDailyController {
    @Autowired
    private StatisticsDailyService dailyService;

    //统计某一天注册人数
    @GetMapping("registerCount/{day}")
    public R countRegister(@PathVariable String day){
        dailyService.countRegister(day);
        return R.ok();
    }

    //图表显示 日期数组 数量数组
    @GetMapping("showData/{type}/{begin}/{end}")
    public R showData(@PathVariable String type, @PathVariable String begin,
                      @PathVariable String end){
        Map<String, Object> map = dailyService.getShowData(type, begin, end);
        return R.ok().data(map);
    }
}

