package com.stacksg.staservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.stacksg.commonutils.R;
import com.stacksg.staservice.client.UCenterClient;
import com.stacksg.staservice.entity.StatisticsDaily;
import com.stacksg.staservice.mapper.StatisticsDailyMapper;
import com.stacksg.staservice.service.StatisticsDailyService;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 网站统计日数据 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-09-16
 */
@Service
public class StatisticsDailyServiceImpl extends ServiceImpl<StatisticsDailyMapper, StatisticsDaily> implements StatisticsDailyService {
    @Autowired
    UCenterClient uCenterClient;
    @Override
    public void countRegister(String day) {
        //添加之前删除相同日期的记录
        QueryWrapper<StatisticsDaily> wrapper = new QueryWrapper<>();
        wrapper.eq("date_calculated", day);
        baseMapper.delete(wrapper);

        R countRegister = uCenterClient.countRegister(day);
        Integer registerNum = (Integer) countRegister.getData().get("countRegister");
        Integer loginNum = RandomUtils.nextInt(100, 200);//TODO
        Integer videoViewNum = RandomUtils.nextInt(100, 200);//TODO
        Integer courseNum = RandomUtils.nextInt(100, 200);//TODO
        //创建统计对象
        StatisticsDaily daily = new StatisticsDaily();
        daily.setRegisterNum(registerNum);
        daily.setLoginNum(loginNum);
        daily.setVideoViewNum(videoViewNum);
        daily.setCourseNum(courseNum);
        daily.setDateCalculated(day);
        baseMapper.insert(daily);
    }

    @Override
    public Map<String, Object> getShowData(String type, String begin, String end) {
        QueryWrapper<StatisticsDaily> wrapper = new QueryWrapper<>();
        wrapper.between("date_calculated", begin, end);
        wrapper.select("date_calculated", type);
        List<StatisticsDaily> statisticsDailyList = baseMapper.selectList(wrapper);

        List<String> dateList = new ArrayList<>();
        List<Integer> numList = new ArrayList<>();

        for (StatisticsDaily s : statisticsDailyList){
            dateList.add(s.getDateCalculated());
            switch (type){
                case "register_num":
                    numList.add(s.getRegisterNum());
                    break;
                case "login_num":
                    numList.add(s.getLoginNum());
                    break;
                case "video_view_num":
                    numList.add(s.getVideoViewNum());
                    break;
                case "course_num":
                    numList.add(s.getCourseNum());
                    break;
            }
        }

        Map<String, Object> map = new HashMap<>();
        map.put("dateList", dateList);
        map.put("numList", numList);
        return map;
    }
}
