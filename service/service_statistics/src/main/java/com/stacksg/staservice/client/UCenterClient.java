package com.stacksg.staservice.client;

import com.stacksg.commonutils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient("service-ucenter")
public interface UCenterClient {
    @GetMapping("/eduucenter/ucenter-member/countRegister/{day}")
    public R countRegister(@PathVariable("day") String day);
}
