package com.stacksg.staservice.mapper;

import com.stacksg.staservice.entity.StatisticsDaily;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 网站统计日数据 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2020-09-16
 */
public interface StatisticsDailyMapper extends BaseMapper<StatisticsDaily> {

}
