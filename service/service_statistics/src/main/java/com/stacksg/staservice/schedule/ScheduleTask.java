package com.stacksg.staservice.schedule;

import com.stacksg.staservice.service.StatisticsDailyService;
import com.stacksg.staservice.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class ScheduleTask {
    @Autowired
    private StatisticsDailyService dailyService;

    //在每天凌晨一点生成前一天统计数据
    @Scheduled(cron = "0 0 1 * * ?")
    public void task2(){
        dailyService.countRegister(DateUtil.formatDate(DateUtil.addDays(new Date(), -1)));
    }
}
