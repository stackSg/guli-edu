package com.stacksg.eduorder.service;

import com.stacksg.eduorder.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单 服务类
 * </p>
 *
 * @author testjava
 * @since 2020-09-15
 */
public interface OrderService extends IService<Order> {

    String createOrder(String courseId, String id);
}
