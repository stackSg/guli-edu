package com.stacksg.eduorder.mapper;

import com.stacksg.eduorder.entity.PayLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 支付日志表 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2020-09-15
 */
public interface PayLogMapper extends BaseMapper<PayLog> {

}
