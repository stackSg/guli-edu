package com.stacksg.eduorder.mapper;

import com.stacksg.eduorder.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2020-09-15
 */
public interface OrderMapper extends BaseMapper<Order> {

}
