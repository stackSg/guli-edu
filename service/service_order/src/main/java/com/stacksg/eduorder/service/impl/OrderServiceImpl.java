package com.stacksg.eduorder.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.stacksg.commonutils.ordervo.CourseWebVoOrder;
import com.stacksg.commonutils.ordervo.UcenterMemberOrder;
import com.stacksg.eduorder.client.EduClient;
import com.stacksg.eduorder.client.UcenterClient;
import com.stacksg.eduorder.entity.Order;
import com.stacksg.eduorder.mapper.OrderMapper;
import com.stacksg.eduorder.service.OrderService;
import com.stacksg.eduorder.utils.OrderNoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-09-15
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {
    @Autowired
    private EduClient eduClient;
    @Autowired
    private UcenterClient ucenterClient;
    @Override
    public String createOrder(String courseId, String id) {
        //通过远程调用获取用户信息
        UcenterMemberOrder userInfo = ucenterClient.getUserInfo(id);
        //通过远程调用获取课程信息
        CourseWebVoOrder courseInfoOrder = eduClient.getCourseInfoOrder(courseId);

        Order order = new Order();
        //生成订单号
        order.setOrderNo(OrderNoUtil.getOrderNo());
        order.setCourseId(courseId);
        order.setCourseTitle(courseInfoOrder.getTitle());
        order.setCourseCover(courseInfoOrder.getCover());
        order.setTeacherName(courseInfoOrder.getTeacherName());
        order.setTotalFee(courseInfoOrder.getPrice());
        order.setMemberId(id);
        order.setMobile(userInfo.getMobile());
        order.setNickname(userInfo.getNickname());
        order.setStatus(0);
        order.setPayType(1);
        baseMapper.insert(order);
        return order.getOrderNo();
    }
}
