package com.stacksg.eduorder.controller;


import com.stacksg.commonutils.R;
import com.stacksg.eduorder.service.PayLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 支付日志表 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2020-09-15
 */
@RestController
@RequestMapping("/eduorder/paylog")
@CrossOrigin
public class PayLogController {
    @Autowired
    private PayLogService payLogService;

    @GetMapping("createNative/{orderNo}")
    //生成支付二维码
    public R createNative(@PathVariable String orderNo){
        Map<String, Object> map = payLogService.createNative(orderNo);
        System.out.println("返回二维码:" + map);
        return R.ok().data(map);
    }

    @GetMapping("queryPayStatus/{orderNo}")
    public R queryPayStatus(@PathVariable String orderNo){
        Map<String, String> map = payLogService.queryPayStatus(orderNo);
        System.out.println("支付状态:" + map);
        if (map == null){
            return R.error().message("支付出错了");
        }
        //判断是否支付成功
        if (map.get("trade_state").equals("SUCCESS")){
            payLogService.updateOrdersStatus(map);
            return R.ok().message("支付成功");
        }

        return R.ok().code(25000).message("支付中");
    }
}

