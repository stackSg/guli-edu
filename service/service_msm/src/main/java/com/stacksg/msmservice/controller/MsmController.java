package com.stacksg.msmservice.controller;

import com.stacksg.commonutils.R;
import com.stacksg.msmservice.service.MsmService;
import com.stacksg.msmservice.utils.RandomUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/edumsm/msm")
@CrossOrigin
public class MsmController {
    @Autowired
    private MsmService msmService;
    @Autowired
    private RedisTemplate redisTemplate;

    @GetMapping("send/{phone}")
    public R send(@PathVariable String phone){
        //设置验证码失效时间，防止频繁发送
        String codeNum = (String)redisTemplate.opsForValue().get(phone);
        if (!StringUtils.isEmpty(codeNum)){
            return R.ok();
        }
        //生成随机验证码
        String code = RandomUtil.getFourBitRandom();
        Map<String, String> map = new HashMap<>();
        map.put("code", code);
        boolean isSend = msmService.send(map, phone);
        if (isSend) {
            //将发送成功的验证码缓存到redis中
            redisTemplate.opsForValue().set(phone, code, 5, TimeUnit.MINUTES);
            return R.ok();
        }else {
            return R.error().message("发送验证码失败");
        }
    }
}
