package com.stacksg.msmservice.service;

import java.util.Map;

public interface MsmService {
    boolean send(Map<String, String> map, String phone);
}
