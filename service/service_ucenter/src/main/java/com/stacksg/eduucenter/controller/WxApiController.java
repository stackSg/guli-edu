package com.stacksg.eduucenter.controller;

import com.google.gson.Gson;
import com.stacksg.commonutils.JwtUtils;
import com.stacksg.eduucenter.entity.UcenterMember;
import com.stacksg.eduucenter.service.UcenterMemberService;
import com.stacksg.eduucenter.utils.HttpClientUtils;
import com.stacksg.servicebase.exceptionhandler.GuliException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;

@Controller
@RequestMapping("/api/ucenter/wx")
@CrossOrigin
public class WxApiController {
    @Value("${wx.open.app_id}")
    private String appId;
    @Value("${wx.open.app_secret}")
    private String appSecret;
    @Value("${wx.open.redirect_url}")
    private String redirectUrl;

    @Autowired
    private UcenterMemberService memberService;

    @GetMapping("login")
    //请求code
    public String getVxCode(){
        // 微信开放平台授权baseUrl
        String baseUrl = "https://open.weixin.qq.com/connect/qrconnect" +
                "?appid=%s" +
                "&redirect_uri=%s" +
                "&response_type=code" +
                "&scope=snsapi_login" +
                "&state=%s" +
                "#wechat_redirect";
        String encodeUrl = "";
        try {
            encodeUrl = URLEncoder.encode(redirectUrl, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url = String.format(baseUrl, appId, encodeUrl, "stacksg");

        return "redirect:" + url;
    }

    @GetMapping("callback")
    //获取code的回调地址
    public String callback(String code, String state){
        //获取code值，类似于验证码
        //通过code获取access_token
        String baseAccessTokenUrl =
                "https://api.weixin.qq.com/sns/oauth2/access_token" +
                        "?appid=%s" +
                        "&secret=%s" +
                        "&code=%s" +
                        "&grant_type=authorization_code";

        String accessTokenUrl = String.format(baseAccessTokenUrl, appId, appSecret, code);
        try {
            //获取返回的json字符串
            String accessTokenInfo = HttpClientUtils.get(accessTokenUrl);
            //解析得到access_token和openid
            Gson gson = new Gson();
            HashMap map = gson.fromJson(accessTokenInfo, HashMap.class);
            String accessToken = (String)map.get("access_token");
            String openid = (String)map.get("openid");

            //判断数据库是否已经存在此openid
            UcenterMember member = memberService.getOpenIdMember(openid);
            if (member == null){

                //使用accessToken和openid请求得到扫描人信息
                String baseUserInfoUrl = "https://api.weixin.qq.com/sns/userinfo" +
                        "?access_token=%s" +
                        "&openid=%s";
                String userInfoUrl = String.format(baseUserInfoUrl, accessToken, openid);
                String userInfo = HttpClientUtils.get(userInfoUrl);

                //解析返回json结果得到扫码人信息
                HashMap userInfoMap = gson.fromJson(userInfo, HashMap.class);
                String nickname = (String) userInfoMap.get("nickname");
                String headimgurl = (String) userInfoMap.get("headimgurl");

                //把扫码人信息加入数据库
                member = new UcenterMember();
                member.setOpenid(openid);
                member.setNickname(nickname);
                member.setAvatar(headimgurl);
                memberService.save(member);
            }
            //使用jwt根据member对象生成token字符串
            String token = JwtUtils.getJwtToken(member.getId(), member.getNickname());
            return "redirect:http://localhost:3000?token=" + token;
        } catch (Exception e) {
            throw new GuliException(20001, "登陆失败");
        }
    }
}
