package com.stacksg.eduucenter.controller;


import com.stacksg.commonutils.JwtUtils;
import com.stacksg.commonutils.R;
import com.stacksg.commonutils.ordervo.UcenterMemberOrder;
import com.stacksg.eduucenter.entity.RegisterVo;
import com.stacksg.eduucenter.entity.UcenterMember;
import com.stacksg.eduucenter.service.UcenterMemberService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 会员表 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2020-09-14
 */
@RestController
@RequestMapping("/eduucenter/ucenter-member")
@CrossOrigin
public class UcenterMemberController {
    @Autowired
    private UcenterMemberService memberService;
    @PostMapping("login")
    public R login(@RequestBody UcenterMember member){
        String token = memberService.login(member);
        return R.ok().data("token", token);
    }

    @PostMapping("register")
    public R register(@RequestBody RegisterVo registerVo){
        memberService.register(registerVo);
        return R.ok();
    }

    @GetMapping("getUserInfo")
    public R getUserInfoByToken(HttpServletRequest request){
        String id = JwtUtils.getMemberIdByJwtToken(request);
        UcenterMember member = memberService.getById(id);
        return R.ok().data("userInfo", member);
    }

    @GetMapping("getUserInfo/{id}")
    public UcenterMemberOrder getUserInfo(@PathVariable String id){
        UcenterMember member = memberService.getById(id);
        UcenterMemberOrder memberOrder = new UcenterMemberOrder();
        BeanUtils.copyProperties(member, memberOrder);
        return memberOrder;
    }

    //查询某一天注册人数
    @GetMapping("countRegister/{day}")
    public R countRegister(@PathVariable String day){
        Integer count = memberService.countRegister(day);
        return R.ok().data("countRegister", count);
    }
}

