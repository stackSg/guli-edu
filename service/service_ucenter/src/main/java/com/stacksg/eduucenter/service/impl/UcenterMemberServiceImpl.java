package com.stacksg.eduucenter.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.stacksg.commonutils.JwtUtils;
import com.stacksg.commonutils.MD5;
import com.stacksg.eduucenter.entity.RegisterVo;
import com.stacksg.eduucenter.entity.UcenterMember;
import com.stacksg.eduucenter.mapper.UcenterMemberMapper;
import com.stacksg.eduucenter.service.UcenterMemberService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.stacksg.servicebase.exceptionhandler.GuliException;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-09-14
 */
@Service
public class UcenterMemberServiceImpl extends ServiceImpl<UcenterMemberMapper, UcenterMember> implements UcenterMemberService {
    @Autowired
    UcenterMemberMapper ucenterMemberMapper;
    @Autowired
    RedisTemplate redisTemplate;
    @Override
    public String login(UcenterMember member) {
        String mobile = member.getMobile();
        String password = member.getPassword();
        if (StringUtils.isEmpty(mobile) || StringUtils.isEmpty(password)){
            throw new GuliException(20001, "登陆失败");
        }

        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile", mobile);
        UcenterMember ucenterMember = ucenterMemberMapper.selectOne(wrapper);

        if (ucenterMember == null){
            throw new GuliException(20001, "登陆失败");
        }
        if (!ucenterMember.getPassword().equals(MD5.encrypt(password))){
            throw new GuliException(20001, "登陆失败");
        }
        if (ucenterMember.getIsDisabled()){
            throw new GuliException(20001, "登陆失败");
        }
        String token = JwtUtils.getJwtToken(ucenterMember.getId(), ucenterMember.getNickname());
        return token;
    }

    @Override
    public void register(RegisterVo registerVo) {
        if (registerVo == null){
            throw new GuliException(20001, "注册失败");
        }
        String code = registerVo.getCode();
        String mobile = registerVo.getMobile();
        String nickname = registerVo.getNickname();
        String password = registerVo.getPassword();

        if (StringUtils.isEmpty(code)|| StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(nickname) || StringUtils.isEmpty(password)){
            throw new GuliException(20001, "注册失败");
        }

        Integer redisCode = (Integer)redisTemplate.opsForValue().get(mobile);
        if (redisCode == null || !code.equals(redisCode.toString())){
            throw new GuliException(20001, "注册失败");
        }

        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile", mobile);
        Integer count = baseMapper.selectCount(wrapper);
        if (count > 0){
            throw new GuliException(20001, "注册失败");
        }

        UcenterMember ucenterMember = new UcenterMember();
        ucenterMember.setMobile(mobile);
        ucenterMember.setNickname(nickname);
        ucenterMember.setPassword(MD5.encrypt(password));
        ucenterMember.setIsDisabled(false);
        ucenterMember.setAvatar("https://my-guliedu.oss-cn-beijing.aliyuncs.com/2020/09/10/bd10418d737c48688c38b031be8cd539file.png");

        baseMapper.insert(ucenterMember);
    }

    @Override
    public UcenterMember getOpenIdMember(String openid) {
        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("openid", openid);
        UcenterMember ucenterMember = baseMapper.selectOne(wrapper);
        return ucenterMember;
    }

    @Override
    public Integer countRegister(String day) {
        return baseMapper.countRegisterDay(day);
    }
}
