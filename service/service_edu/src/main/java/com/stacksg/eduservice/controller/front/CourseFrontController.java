package com.stacksg.eduservice.controller.front;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.stacksg.commonutils.JwtUtils;
import com.stacksg.commonutils.R;
import com.stacksg.commonutils.ordervo.CourseWebVoOrder;
import com.stacksg.eduservice.client.OrdersClient;
import com.stacksg.eduservice.entity.EduCourse;
import com.stacksg.eduservice.entity.chapter.ChapterVo;
import com.stacksg.eduservice.entity.frontvo.CourseFrontVo;
import com.stacksg.eduservice.entity.frontvo.CourseWebVo;
import com.stacksg.eduservice.service.EduChapterService;
import com.stacksg.eduservice.service.EduCourseService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/eduservice/coursefront")
@CrossOrigin
public class CourseFrontController {
    @Autowired
    private EduCourseService courseService;
    @Autowired
    private EduChapterService chapterService;
    @Autowired
    private OrdersClient ordersClient;

    @PostMapping("getCourseList/{page}/{limit}")
    public R getCourseList(@PathVariable long page, @PathVariable long limit,
                           @RequestBody(required = false) CourseFrontVo courseFrontVo){
        Page<EduCourse> coursePage = new Page<>(page, limit);
        Map<String, Object> map = courseService.getCourseListFront(coursePage, courseFrontVo);
        return R.ok().data(map);
    }

    @GetMapping("getCourseInfo/{courseId}")
    public R getCourseInfo(@PathVariable String courseId, HttpServletRequest request){
        //查询课程基本信息
        CourseWebVo courseWebVo = courseService.getCourseBaseInfo(courseId);
        //查询课程章节和小结信息
        List<ChapterVo> chapterVideos = chapterService.getChapterVideoByCourseId(courseId);
        //根据课程id和用户id查询当前课程是否已经支付
        boolean buyCourse = ordersClient.isBuyCourse(courseId, JwtUtils.getMemberIdByJwtToken(request));
        return R.ok().data("courseWebVo", courseWebVo).data("chapterVideoList", chapterVideos)
                .data("isBuy", buyCourse);
    }

    @GetMapping("getCourseInfoOrder/{courseId}")
    public CourseWebVoOrder getCourseInfoOrder(@PathVariable String courseId){
        CourseWebVo courseBaseInfo = courseService.getCourseBaseInfo(courseId);
        CourseWebVoOrder courseWebVoOrder = new CourseWebVoOrder();
        BeanUtils.copyProperties(courseBaseInfo, courseWebVoOrder);
        return courseWebVoOrder;
    }
}
