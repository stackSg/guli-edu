package com.stacksg.eduservice.service;

import com.stacksg.eduservice.entity.EduSubject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.stacksg.eduservice.entity.subject.OneSubject;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 服务类
 * </p>
 *
 * @author testjava
 * @since 2020-09-10
 */
public interface EduSubjectService extends IService<EduSubject> {

    void saveSubject(MultipartFile file, EduSubjectService subjectService);

    List<OneSubject> getAllSubject();
}
