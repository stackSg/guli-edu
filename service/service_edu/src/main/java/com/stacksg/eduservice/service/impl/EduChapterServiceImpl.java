package com.stacksg.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.stacksg.eduservice.entity.EduChapter;
import com.stacksg.eduservice.entity.EduVideo;
import com.stacksg.eduservice.entity.chapter.ChapterVo;
import com.stacksg.eduservice.entity.chapter.VideoVo;
import com.stacksg.eduservice.mapper.EduChapterMapper;
import com.stacksg.eduservice.service.EduChapterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.stacksg.eduservice.service.EduVideoService;
import com.stacksg.servicebase.exceptionhandler.GuliException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-09-10
 */
@Service
public class EduChapterServiceImpl extends ServiceImpl<EduChapterMapper, EduChapter> implements EduChapterService {
    @Autowired
    private EduVideoService videoService;
    @Override
    public List<ChapterVo> getChapterVideoByCourseId(String courseId) {
        //查询章节
        QueryWrapper<EduChapter> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id", courseId);
        List<EduChapter> chapterList = baseMapper.selectList(wrapper);
        //查询小结
        QueryWrapper<EduVideo> wrapper2 = new QueryWrapper<>();
        wrapper.eq("course_id", courseId);
        List<EduVideo> videoList = videoService.list(wrapper2);
        //讲章节和小结组装返回
        List<ChapterVo> res = new ArrayList<>();
        for (EduChapter chapter : chapterList){
            ChapterVo chapterVo = new ChapterVo();
            BeanUtils.copyProperties(chapter, chapterVo);
            for (EduVideo video : videoList){
                if (video.getChapterId().equals(chapter.getId())){
                    VideoVo videoVo = new VideoVo();
                    BeanUtils.copyProperties(video, videoVo);
                    chapterVo.getChildren().add(videoVo);
                }
            }
            res.add(chapterVo);
        }
        return res;
    }

    @Override
    public boolean deleteChapter(String chapterId) {
        QueryWrapper<EduVideo> wrapper = new QueryWrapper<>();
        wrapper.eq("chapter_id", chapterId);
        int count = videoService.count(wrapper);
        if (count > 0){
            throw new GuliException(20001, "不能删除");
        }else {
            int res = baseMapper.deleteById(chapterId);
            return res > 0;
        }
    }

    @Override
    public void removeChapterByCourseId(String courseId) {
        QueryWrapper<EduChapter> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id", courseId);
        baseMapper.delete(wrapper);
    }
}
