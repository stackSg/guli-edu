package com.stacksg.eduservice.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.stacksg.eduservice.entity.EduCourse;
import com.baomidou.mybatisplus.extension.service.IService;
import com.stacksg.eduservice.entity.frontvo.CourseFrontVo;
import com.stacksg.eduservice.entity.frontvo.CourseWebVo;
import com.stacksg.eduservice.entity.vo.CourseInfoVo;
import com.stacksg.eduservice.entity.vo.CoursePublishVo;

import java.util.Map;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author testjava
 * @since 2020-09-10
 */
public interface EduCourseService extends IService<EduCourse> {

    String saveCourseInfo(CourseInfoVo courseInfoVo);

    CourseInfoVo getCourseInfo(String courseId);

    void updateCourseInfo(CourseInfoVo courseInfoVo);

    CoursePublishVo publishCourseInfo(String id);

    void removeCourse(String courseId);

    Map<String, Object> getCourseListFront(Page<EduCourse> coursePage, CourseFrontVo courseFrontVo);

    CourseWebVo getCourseBaseInfo(String courseId);
}
