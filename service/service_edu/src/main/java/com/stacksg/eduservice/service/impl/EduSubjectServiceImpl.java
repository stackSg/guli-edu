package com.stacksg.eduservice.service.impl;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.stacksg.eduservice.entity.EduSubject;
import com.stacksg.eduservice.entity.excel.SubjectData;
import com.stacksg.eduservice.entity.subject.OneSubject;
import com.stacksg.eduservice.listener.SubjectExcelListener;
import com.stacksg.eduservice.mapper.EduSubjectMapper;
import com.stacksg.eduservice.service.EduSubjectService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程科目 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-09-10
 */
@Service
public class EduSubjectServiceImpl extends ServiceImpl<EduSubjectMapper, EduSubject> implements EduSubjectService {

    @Override
    public void saveSubject(MultipartFile file, EduSubjectService subjectService) {
        try {
            InputStream inputStream = file.getInputStream();
            EasyExcel.read(inputStream, SubjectData.class, new SubjectExcelListener(subjectService)).sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<OneSubject> getAllSubject() {
        QueryWrapper<EduSubject> wrapper = new QueryWrapper<>();
        wrapper.eq("parent_id", 0);
        List<EduSubject> subjects = baseMapper.selectList(wrapper);

        QueryWrapper<EduSubject> wrapper2 = new QueryWrapper<>();
        wrapper.ne("parent_id", 0);
        List<EduSubject> subjects2 = baseMapper.selectList(wrapper2);

        List<OneSubject> res = new ArrayList<>();
        for (EduSubject sub : subjects){
            OneSubject oneSubject = new OneSubject();
            BeanUtils.copyProperties(sub, oneSubject);
            res.add(oneSubject);
            for (EduSubject sub2 : subjects2){
                if (sub2.getParentId().equals(sub.getId())){
                    OneSubject twoSubject = new OneSubject();
                    BeanUtils.copyProperties(sub2, twoSubject);
                    oneSubject.getChildren().add(twoSubject);
                }
            }
        }
        return res;
    }
}
