package com.stacksg.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.stacksg.eduservice.entity.EduCourse;
import com.stacksg.eduservice.entity.EduCourseDescription;
import com.stacksg.eduservice.entity.frontvo.CourseFrontVo;
import com.stacksg.eduservice.entity.frontvo.CourseWebVo;
import com.stacksg.eduservice.entity.vo.CourseInfoVo;
import com.stacksg.eduservice.entity.vo.CoursePublishVo;
import com.stacksg.eduservice.mapper.EduCourseMapper;
import com.stacksg.eduservice.service.EduChapterService;
import com.stacksg.eduservice.service.EduCourseDescriptionService;
import com.stacksg.eduservice.service.EduCourseService;
import com.stacksg.eduservice.service.EduVideoService;
import com.stacksg.servicebase.exceptionhandler.GuliException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-09-10
 */
@Service
public class EduCourseServiceImpl extends ServiceImpl<EduCourseMapper, EduCourse> implements EduCourseService {
    @Autowired
    private EduCourseDescriptionService descriptionService;
    @Autowired
    private EduVideoService eduVideoService;
    @Autowired
    private EduChapterService chapterService;
    @Override
    public String saveCourseInfo(CourseInfoVo courseInfoVo) {
        //向课程表添加课程基本信息
        EduCourse eduCourse = new EduCourse();
        BeanUtils.copyProperties(courseInfoVo, eduCourse);
        int insert = baseMapper.insert(eduCourse);
        if (insert <= 0){
            throw new GuliException(20001, "添加课程信息失败");
        }
        String cid = eduCourse.getId();
        //向课程描述表添加描述
        EduCourseDescription description = new EduCourseDescription();
        BeanUtils.copyProperties(courseInfoVo, description);
        description.setId(cid);
        descriptionService.save(description);

        return cid;
    }

    @Override
    public CourseInfoVo getCourseInfo(String courseId) {
        //查询课程信息
        EduCourse course = baseMapper.selectById(courseId);
        //查询课程描述
        EduCourseDescription description = descriptionService.getById(course);
        CourseInfoVo courseInfoVo = new CourseInfoVo();
        BeanUtils.copyProperties(course, courseInfoVo);
        courseInfoVo.setDescription(description.getDescription());
        return courseInfoVo;
    }

    //修改课程信息
    @Override
    public void updateCourseInfo(CourseInfoVo courseInfoVo) {
        EduCourse eduCourse = new EduCourse();
        BeanUtils.copyProperties(courseInfoVo, eduCourse);
        int update = baseMapper.updateById(eduCourse);
        if (update == 0){
            throw new GuliException(20001, "修改课程信息失败");
        }
        EduCourseDescription description = new EduCourseDescription();
        BeanUtils.copyProperties(courseInfoVo, description);
        descriptionService.updateById(description);
    }

    @Override
    public CoursePublishVo publishCourseInfo(String id) {
        return baseMapper.getPublishCourseInfo(id);
    }

    @Override
    public void removeCourse(String courseId) {
        //根据课程id删除小结
        eduVideoService.removeVideoByCourseId(courseId);
        //根据课程id删除章节
        chapterService.removeChapterByCourseId(courseId);
        //根据课程id删除描述
        descriptionService.removeById(courseId);
        //删除课程
        int res = baseMapper.deleteById(courseId);
        if (res == 0){
            throw new GuliException(20001, "删除课程失败");
        }
    }

    @Override
    public Map<String, Object> getCourseListFront(Page<EduCourse> pageParam, CourseFrontVo courseQuery) {
        QueryWrapper<EduCourse> queryWrapper = new QueryWrapper<>();
        if (courseQuery != null){
            if (!StringUtils.isEmpty(courseQuery.getSubjectParentId())) {
                queryWrapper.eq("subject_parent_id",
                        courseQuery.getSubjectParentId());
            }
            if (!StringUtils.isEmpty(courseQuery.getSubjectId())) {
                queryWrapper.eq("subject_id", courseQuery.getSubjectId());
            }
            if (!StringUtils.isEmpty(courseQuery.getBuyCountSort())) {
                queryWrapper.orderByDesc("buy_count");
            }
            if (!StringUtils.isEmpty(courseQuery.getGmtCreateSort())) {
                queryWrapper.orderByDesc("gmt_create");
            }
            if (!StringUtils.isEmpty(courseQuery.getPriceSort())) {
                queryWrapper.orderByDesc("price");
            }
        }
        baseMapper.selectPage(pageParam, queryWrapper);
        List<EduCourse> records = pageParam.getRecords();
        long current = pageParam.getCurrent();
        long pages = pageParam.getPages();
        long size = pageParam.getSize();
        long total = pageParam.getTotal();
        boolean hasNext = pageParam.hasNext();
        boolean hasPrevious = pageParam.hasPrevious();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("items", records);
        map.put("current", current);
        map.put("pages", pages);
        map.put("size", size);
        map.put("total", total);
        map.put("hasNext", hasNext);
        map.put("hasPrevious", hasPrevious);

        return map;
    }

    @Override
    public CourseWebVo getCourseBaseInfo(String courseId) {
        return baseMapper.getCourseBaseInfo(courseId);
    }
}
