package com.stacksg.eduservice.mapper;

import com.stacksg.eduservice.entity.EduCourse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.stacksg.eduservice.entity.frontvo.CourseWebVo;
import com.stacksg.eduservice.entity.vo.CoursePublishVo;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2020-09-10
 */

public interface EduCourseMapper extends BaseMapper<EduCourse> {
    CoursePublishVo getPublishCourseInfo(String courseId);

    CourseWebVo getCourseBaseInfo(String courseId);
}
