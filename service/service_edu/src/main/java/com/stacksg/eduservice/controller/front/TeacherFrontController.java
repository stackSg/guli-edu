package com.stacksg.eduservice.controller.front;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.stacksg.commonutils.R;
import com.stacksg.eduservice.entity.EduCourse;
import com.stacksg.eduservice.entity.EduTeacher;
import com.stacksg.eduservice.service.EduCourseService;
import com.stacksg.eduservice.service.EduTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/eduservice/teacherfront")
@CrossOrigin
public class TeacherFrontController {

    @Autowired
    private EduTeacherService teacherService;
    @Autowired
    private EduCourseService courseService;

    @GetMapping("/getTeacherFrontList/{page}/{limit}")
    public R getTeacherFrontList(@PathVariable long page, @PathVariable long limit){
        Page<EduTeacher> teacherPage = new Page<>(page, limit);
        Map<String, Object> map =  teacherService.getTeacherFrontList(teacherPage);
        return R.ok().data(map);
    }

    @GetMapping("/getTeacherFrontInfo/{id}")
    public R getTeacherFrontInfo(@PathVariable String id){
        //得到讲师
        EduTeacher teacher = teacherService.getById(id);
        //得到讲师所讲课程
        QueryWrapper<EduCourse> wrapper = new QueryWrapper<>();
        wrapper.eq("teacher_id", id);
        List<EduCourse> courseList = courseService.list(wrapper);
        return R.ok().data("teacher", teacher).data("courseList", courseList);
    }
}
