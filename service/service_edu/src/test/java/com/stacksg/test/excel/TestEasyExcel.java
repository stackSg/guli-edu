package com.stacksg.test.excel;

import com.alibaba.excel.EasyExcel;

public class TestEasyExcel {
    public static void main(String[] args) {
        String filename = "f://test.xlsx";
//        List<Person> list = new ArrayList<>();
//        for (int i = 0; i < 10; i++) {
//            Person p = new Person();
//            p.setNo(i);
//            p.setName("a" + i);
//            list.add(p);
//        }
//        EasyExcel.write(filename, Person.class).sheet("列表").doWrite(list);
        EasyExcel.read(filename, Person.class, new ExcelListener()).sheet().doRead();
    }
}
