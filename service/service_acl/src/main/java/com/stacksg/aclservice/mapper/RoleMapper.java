package com.stacksg.aclservice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.stacksg.aclservice.entity.Role;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2020-01-12
 */
public interface RoleMapper extends BaseMapper<Role> {

}
