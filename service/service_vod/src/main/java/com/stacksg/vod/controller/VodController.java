package com.stacksg.vod.controller;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.vod.model.v20170321.DeleteVideoRequest;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthRequest;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthResponse;
import com.stacksg.commonutils.R;
import com.stacksg.servicebase.exceptionhandler.GuliException;
import com.stacksg.vod.service.VodService;
import com.stacksg.vod.utils.InitVodClient;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/eduvod/video")
@CrossOrigin
public class VodController {
    @Value("${aliyun.vod.file.keyid}")
    private String accessKeyId;
    @Value("${aliyun.vod.file.keysecret}")
    private String accessKeySecret;


    @Autowired
    private VodService vodService;
    //上传视频到阿里云
    @PostMapping("uploadVideo")
    public R uploadVideo(MultipartFile file){
        String vId = vodService.uploadVideo(file);
        return R.ok().data("videoId", vId);
    }

    //根据视频Id删除阿里云视频
    @DeleteMapping("removeVideo/{id}")
    public R removeVideo(@PathVariable String id){
        try {
            DefaultAcsClient acsClient = InitVodClient.initVodClient(accessKeyId, accessKeySecret);
            DeleteVideoRequest request = new DeleteVideoRequest();
            request.setVideoIds(id);
            acsClient.getAcsResponse(request);
            return R.ok();
        }catch (Exception e){
            e.printStackTrace();
            throw new GuliException(20001, "删除阿里云视频失败");
        }
    }

    //删除阿里云多个视频
    @DeleteMapping("deleteBatch")
    public R deleteBatch(@RequestParam("videoIdList") List<String> videoIdList){
        try {
            DefaultAcsClient acsClient = InitVodClient.initVodClient(accessKeyId, accessKeySecret);
            DeleteVideoRequest request = new DeleteVideoRequest();
            String ids = StringUtils.join(videoIdList.toArray(), ",");
            request.setVideoIds(ids);
            acsClient.getAcsResponse(request);
            return R.ok();
        }catch (Exception e){
            e.printStackTrace();
            throw new GuliException(20001, "删除阿里云视频失败");
        }
    }

    //根据视频id获取播放凭证
    @GetMapping("getPlayAuth/{videoId}")
    public R getPlayAuth(@PathVariable String videoId){
        //创建初始化对象
        DefaultAcsClient acsClient = InitVodClient.initVodClient("LTAI4G5YDQPUvTnD8Vxgv4Gd", "3ul3O3q9MTxMTa9ipuQBJuxZUP0qRc");
        GetVideoPlayAuthRequest request = new GetVideoPlayAuthRequest();
        GetVideoPlayAuthResponse response = new GetVideoPlayAuthResponse();

        request.setVideoId(videoId);
        try {
            response = acsClient.getAcsResponse(request);
            return R.ok().data("playAuth", response.getPlayAuth());
        } catch (ClientException e) {
            throw new GuliException(20001, "获取视频播放凭证失败");
        }
    }

    public static void main(String[] args) {
        int[] arr = {7, 8, 9, 4, 5, 6, 1, 2, 3};
        for (int i = 0; i < arr.length-1; i++){
            for (int j = 0; j < arr.length-i-1; j++){
                if (arr[j] > arr[j+1]){
                    int t = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = t;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }
}
