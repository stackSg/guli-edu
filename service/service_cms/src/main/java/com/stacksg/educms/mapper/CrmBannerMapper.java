package com.stacksg.educms.mapper;

import com.stacksg.educms.entity.CrmBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 首页banner表 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2020-09-13
 */
public interface CrmBannerMapper extends BaseMapper<CrmBanner> {

}
